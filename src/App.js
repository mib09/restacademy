import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import { Nav, NavItem, Navbar, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';


import 'bootstrap/dist/css/bootstrap.min.css';
import './components/css/app.css';

import Home from "./components/HomeComponent";
import Alumnos from "./components/alumno/AlumnosComponent";
import NuevoAlumno from './components/alumno/NuevoAlumnoComponent';
import EliminaAlumno from './components/alumno/EliminaAlumnoComponent';
import EditarAlumno from './components/alumno/EditarAlumnoComponent';
import Cursos from './components/curso/CursosComponent';
import NuevoCurso from './components/curso/NuevoCursoComponent';
import EliminaCurso from './components/curso/EliminaCursoComponent';
import EditarCurso from './components/curso/EditarCursoComponent';

import P404 from './components/P404';

import CursoData from './components/curso/CursoData';
import AlumnoData from "./components/alumno/AlumnoData";

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      alumnos: [],
      ultimoAlumno: 0,
      cursos: [],
      ultimoCurso: 0
    }

    this.cargarDatos = this.cargarDatos.bind(this);
    this.guardaAlumno = this.guardaAlumno.bind(this);
    this.eliminaAlumno = this.eliminaAlumno.bind(this);
    this.guardaCurso = this.guardaCurso.bind(this);
    this.eliminaCurso = this.eliminaCurso.bind(this);
    this.loadData = this.loadData.bind(this);
    this.cargarDatos();
  }

  cargarDatos() {
    const alumnosInicio = [
      new AlumnoData(1, "Fulano", "fu@lano.com", "M", 20),
      new AlumnoData(2, "Fulano", "fu@lano.com", "M", 20),
      new AlumnoData(3, "Fulano", "fu@lano.com", "M", 20)
    ];

    const cursosInicio = [
      new CursoData(1, "Java", "IT"),
      new CursoData(2, "Javascript", "IT"),
      new CursoData(3, "C#", "IT")
    ];


    this.state = {
      alumnos: alumnosInicio,
      cursos: cursosInicio,
      ultimoAlumno: 3,
      ultimoCurso: 3
    };
  }

  guardaAlumno(datos) {
    console.log("Guardando Alumno")
    //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
    if (datos.id === 0) {
      datos.id = this.state.alumnos.length + 1;
      this.setState({ ultimoAlumno: datos.id });
    }
    // comprobaciones adicionales... email ya existe? datos llenos?
    // si todo ok creamos contacto y lo añadimos a la lista
    let nuevo = new AlumnoData(datos.id, datos.name, datos.email, datos.gender, datos.age);
    // si contacto existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo contacto o desde modifica contacto
    let nuevaLista = this.state.alumnos.filter(el => el.id !== nuevo.id);
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);
    // finalmente actualizamos state
    this.setState({ alumnos: nuevaLista });
    console.log("Nueva lista" + nuevaLista.length);
  }

  eliminaAlumno(id) {
    let nuevaLista = this.state.alumnos.filter(el => el.id !== id);
    console.log("borro id " + id + " length lista: " + nuevaLista.length)

    for (let j = id - 1; j < nuevaLista.length; j++) {
      nuevaLista[j].id--;
    }
    //asignamos a contactos
    this.setState({ alumnos: nuevaLista });
  }

  guardaCurso(datos) {
    console.log("Guardando Curso")
    if (datos.id === 0) {
      datos.id = this.state.cursos.length + 1;
      this.setState({ ultimoCurso: datos.id });
    }
    let nuevo = new CursoData(datos.id, datos.name, datos.topic);
    let nuevaLista = this.state.cursos.filter(el => el.id !== nuevo.id);
    nuevaLista.push(nuevo);
    this.setState({ cursos: nuevaLista });
    console.log("Nueva lista" + nuevaLista.length);
  }

  eliminaCurso(id) {
    let nuevaLista = this.state.cursos.filter(el => el.id !== id);
    console.log("borro id " + id + " length lista: " + nuevaLista.length)

    for (let j = id - 1; j < nuevaLista.length; j++) {
      nuevaLista[j].id--;
    }
    //asignamos a contactos
    this.setState({ cursos: nuevaLista });
  }

  loadData(dataPath){
    var text = localStorage.getItem(dataPath);
        if (text) {
            var obj = JSON.parse(text);
            console.log(obj);
            this.setState(obj);
        }
  }

  render() {
    return (
      <BrowserRouter>
        <Container>
          <Row>
            <h1> JS Academy</h1>
          </Row>
          <Row className = "col-1">            
            <Navbar  bg="light" expand="lg">
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                  <Link className="nav-link" to="/">Home</Link>
                  <Link className="nav-link" to="/alumnos">Alumnos</Link>
                  <Link className="nav-link" to="/nuevoalumno">Nuevo Alumno</Link>
                  <Link className="nav-link" to="/cursos">Cursos</Link>
                  <Link className="nav-link" to="/nuevocurso">Nuevo Curso</Link>
                  {/* <Nav.Link to="alumnos">Alumnos</Nav.Link>                   */}
                </Nav>                
              </Navbar.Collapse>
            </Navbar>
          </Row>
          <Row>
            {/* <Col xs="12">
              <h1> JS Academy</h1>
              <ul className="list-unstyled inlineMenu">
                <li> <Link to="/">Home</Link> </li>
                <li> <Link to="/alumnos">Alumnos</Link> </li>
                <li> <Link to="/nuevoalumno">Añadir Alumno</Link> </li>
                <li> <Link to="/cursos">Cursos</Link> </li>
                <li> <Link to="/nuevocurso">Añadir Curso</Link> </li>
              </ul>
            </Col> */}
            <Col xs="12">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/alumnos" render={() => <Alumnos alumnos={this.state} loadData = {this.loadData}/>} />
                <Route path="/nuevoalumno" render={() => <NuevoAlumno guardaAlumno={this.guardaAlumno} />} />
                <Route path="/eliminarAlumno/:idAlumno" render={(props) => <EliminaAlumno alumnos={this.state.alumnos} eliminaAlumno={this.eliminaAlumno} {...props} />} />
                <Route path="/editarAlumno/:idAlumno" render={(props) => <EditarAlumno alumnos={this.state.alumnos} guardaAlumno={this.guardaAlumno} {...props} />} />
                <Route path="/cursos" render={() => <Cursos cursos={this.state.cursos} />} />
                <Route path="/nuevocurso" render={() => <NuevoCurso guardaCurso={this.guardaCurso} />} />
                <Route path="/eliminarcurso/:idCurso" render={(props) => <EliminaCurso cursos={this.state.cursos} eliminaCurso={this.eliminaCurso} {...props} />} />
                <Route path="/editarcurso/:idCurso" render={(props) => <EditarCurso cursos={this.state.cursos} guardaCurso={this.guardaCurso} {...props} />} />

                <Route component={P404} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </BrowserRouter >);
  }
}