import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import {NavBar, Nav, NavItem, Image} from 'react-bootstrap';

export default class HomeComponent extends React.Component{

    render(){
        return(
            <>
            <h1>Home</h1>
            <Image src="https://picsum.photos/id/154/1000/200" fluid/>
            </>
        );
    }
}