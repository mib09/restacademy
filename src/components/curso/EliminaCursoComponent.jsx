
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button } from 'reactstrap';


export default class EliminaCursoComponent extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idCurso * 1;
        let actual = this.props.cursos.filter(el => el.id === id)[0];

        this.state = {
            name: actual.name,
            topic: actual.topic,
            id: actual.id,
            back: false
        };

        this.eliminar = this.eliminar.bind(this);
        this.volver = this.volver.bind(this);

    }

    eliminar() {
        this.props.eliminaCurso(this.state.id);
        this.setState({ back: true });
    }


    volver() {
        this.setState({ back: true });
    }

    render() {

        if (this.state.back === true) {
            return <Redirect to='/cursos' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.name}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.volver} color="success">No</Button>
            </>

        );
    }
}

