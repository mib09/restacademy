
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

export default class EditarCursoComponent extends Component {

    constructor(props) {
        super(props);

        let id = this.props.match.params.idCurso * 1;
        let cursoEditar = this.props.cursos.filter(el => el.id === id)[0];
        this.state = {
            id: cursoEditar.id,
            name: cursoEditar.name,
            topic: cursoEditar.topic,
            back: false
        };
        this.inputHandler = this.inputHandler.bind(this);
        this.submit = this.submit.bind(this);
    }

    inputHandler(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(target.value + ": valor: " + target.name);
        this.setState({ [name]: value });
    }

    submit(e) {
        this.props.guardaCurso({
            name: this.state.name,
            topic: this.state.topic,
            id: this.state.id
        });
        e.preventDefault();
        this.setState({ back: true });
    }

    render() {

        if (this.state.back === true) {
            console.log("redirecting");
            return <Redirect to='/cursos' />
        }

        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Nombre</Label>
                            <Input type="text"
                                name="name"
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.inputHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="topicInput">Especialidad</Label>
                            <Input type="text"
                                name="topic"
                                id="topicInput"
                                value={this.state.topic}
                                onChange={this.inputHandler} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>
        )
    }
}