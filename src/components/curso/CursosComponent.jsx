import React from 'react';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Table, Container, Row, Col } from 'reactstrap';
import {NavBar, Nav, NavItem, Image} from 'react-bootstrap';

export default class CursosComponent extends React.Component{
    constructor(props){
        super(props);

    }

    render(){
        let filas = this.props.cursos.sort((a,b) => a.id-b.id).map(a => {
            return (
                <tr key={a.id}>
                    <td>{a.id}</td>
                    <td>{a.name}</td>
                    <td>{a.topic}</td>                   
                    <td><Link to={"/editarCurso/" + a.id}>Editar</Link></td>
                    <td><Link to={"/eliminarCurso/" + a.id}>Eliminar</Link></td>
                </tr>
            );
        })


        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Especialidad</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        );
    
    }
}