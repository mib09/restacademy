import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

export default class NuevoCursoComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            topic: '',
            back: false
        };
        this.inputHandler = this.inputHandler.bind(this);
        this.submit = this.submit.bind(this);
    }

    inputHandler(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(target.value + ": valor: "+ target.name);
        this.setState({ [name]: value });
    }

    submit(e) {
        this.props.guardaCurso({
            name: this.state.name,
            topic: this.state.topic,
            id: 0
        });
        e.preventDefault();
        this.setState({ back: true });
    }

    render() {

        //si se activa volver redirigimos a lista
        if (this.state.back === true) {
            console.log("redirecting");
            return <Redirect to='/cursos' />
        }

        return (

            //se pasa por onSubmit en lugar del onclick
            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Nombre</Label>
                            <Input type="text"
                                name="name"
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.inputHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="topicInput">Especialidad</Label>
                            <Input type="text"
                                name="topic"
                                id="topicInput"
                                value={this.state.topic}
                                onChange={this.inputHandler} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>

        )
    }
}