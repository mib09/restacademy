
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

export default class EditarAlumnoComponent extends Component {

    constructor(props) {
        super(props);

        let id = this.props.match.params.idAlumno * 1;
        let alumnoEditar = this.props.alumnos.filter(el => el.id === id)[0];
        this.state = {
            id: alumnoEditar.id,
            name: alumnoEditar.name,
            email: alumnoEditar.email,
            gender: alumnoEditar.gender,
            age: alumnoEditar.age,
            back: false
        };
        this.inputHandler = this.inputHandler.bind(this);
        this.submit = this.submit.bind(this);
    }

    inputHandler(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(target.value + ": valor: " + target.name);
        this.setState({ [name]: value });
    }

    submit(e) {
        this.props.guardaAlumno({
            name: this.state.name,
            email: this.state.email,
            gender: this.state.gender,
            age: this.state.age,
            id: this.state.id
        });
        e.preventDefault();
        this.setState({ back: true });
    }

    render() {

        if (this.state.back === true) {
            console.log("redirecting");
            return <Redirect to='/alumnos' />
        }

        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Nombre</Label>
                            <Input type="text"
                                name="name"
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.inputHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text"
                                name="email"
                                id="emailInput"
                                value={this.state.email}
                                onChange={this.inputHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="genderInput">Genero</Label>
                            <Input type="text"
                                name="gender"
                                id="genderInput"
                                value={this.state.gender}
                                onChange={this.inputHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="ageInput">Edad</Label>
                            <Input type="number"
                                name="age"
                                id="ageInput"
                                value={this.state.age}
                                onChange={this.inputHandler} />
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>

        )
    }
}