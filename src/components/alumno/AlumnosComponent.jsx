import React from 'react';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Table, Container, Row, Col } from 'reactstrap';
import { NavBar, Nav, NavItem, Image, Button } from 'react-bootstrap';

const dataPath = "dataalumno";
export default class AlumnosComponent extends React.Component {
    constructor(props) {
        super(props);
        this.saveData = this.saveData.bind(this);
        this.loadData = this.loadData.bind(this);
    }
    saveData() {
        console.log("start save")
        var jsonData = JSON.stringify(this.props.alumnos);
        localStorage.setItem(dataPath, jsonData);
        console.log("Saved " + jsonData);
    }

    //carga de datos
    loadData() {
        console.log("Loading Data")
        this.props.loadData(dataPath);
        /*var text = localStorage.getItem("dataalumno");
        if (text) {
            var obj = JSON.parse(text);
            this.setState(obj);
        }*/
    }

    render() {
        let filas = this.props.alumnos.alumnos.sort((a, b) => a.id - b.id).map(a => {
            return (
                <tr key={a.id}>
                    <td>{a.id}</td>
                    <td>{a.name}</td>
                    <td>{a.email}</td>
                    <td>{a.gender}</td>
                    <td>{a.age}</td>
                    <td><Link to={"/editarAlumno/" + a.id}>Editar</Link></td>
                    <td><Link to={"/eliminarAlumno/" + a.id}>Eliminar</Link></td>
                </tr>
            );
        })


        return (
            <>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Genero</th>
                            <th>Edad</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                <Col>
                    <Button onClick = {this.saveData} color="primary">Guardar</Button>
                    <Button onClick = {this.loadData} color="primary">Cargar</Button>
                </Col>
            </>
        );

    }
}