
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button } from 'reactstrap';


export default class EliminaAlumnoComponent extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idAlumno * 1;
        let actual = this.props.alumnos.filter(el => el.id === id)[0];

        this.state = {
            name: actual.name,
            email: actual.email,
            id: actual.id,
            back: false
        };

        this.eliminar = this.eliminar.bind(this);
        this.volver = this.volver.bind(this);

    }

    eliminar() {
        this.props.eliminaAlumno(this.state.id);
        this.setState({ back: true });
    }


    volver() {
        this.setState({ back: true });
    }

    render() {

        if (this.state.back === true) {
            return <Redirect to='/alumnos' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.name}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.volver} color="success">No</Button>
            </>

        );
    }
}

